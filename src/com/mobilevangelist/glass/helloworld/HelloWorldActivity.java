/*
 * Copyright (C) 2013 Mobilevangelist.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.mobilevangelist.glass.helloworld;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.view.KeyEvent;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.glass.app.Card;
import com.google.android.glass.media.Sounds;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Main activity.
 */
public class HelloWorldActivity extends Activity {


  private TextView _mainTextView;
  private ImageView _weatherIconImageView;

  private TextToSpeech _speech;

  private Context _context = this;
    private double mTemp;
    private String mCurrent; //String of current temp

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);



     _speech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
      @Override
      public void onInit(int status) {
          _speech.speak("The weather is" + mCurrent + "Fahrenheit", TextToSpeech.QUEUE_FLUSH, null);
      }
    });

    setContentView(R.layout.layout_helloworld);
    _weatherIconImageView = (ImageView)findViewById(R.id.weatherIconImageView);
    _mainTextView = (TextView)findViewById(R.id.main_text_view);
      new FetchWeather().execute();
  }

  /**
   * Handle the tap event from the touchpad.
   */
  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    switch (keyCode) {
    // Handle tap events.
    case KeyEvent.KEYCODE_DPAD_CENTER:
    case KeyEvent.KEYCODE_ENTER:

      AudioManager audio = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
      audio.playSoundEffect(Sounds.TAP);

        _speech.speak("The weather is" + mCurrent + "Fahrenheit", TextToSpeech.QUEUE_FLUSH, null);

        Drawable drawable = loadImageFromWeb("http://openweathermap.org/img/w/10d.png");
        _weatherIconImageView.setImageDrawable(drawable);

      return true;
    default:
      return super.onKeyDown(keyCode, event);
    }
  }

  @Override
  public void onResume() {
    super.onResume();
  }

  @Override
  public void onPause() {
    super.onPause();
  }

  @Override
  public void onDestroy() {
      _speech.speak("Goodbye", TextToSpeech.QUEUE_FLUSH, null);

      super.onDestroy();
  }


    private class FetchWeather extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            Location loc = getLastLocation(getApplicationContext());


            return new weatherFinder().getWeather();
//            return new weatherFinder(loc).getWeather();
        }

        @Override
        protected void onPostExecute(String str) {

            try {

                JSONObject weatherObject = new JSONObject(str);
                JSONObject temp = weatherObject.getJSONObject("main");
                double currentTemp = temp.getDouble("temp");
                mTemp = (currentTemp - 273.15)*1.800 + 32.00;
                mCurrent = Double.toString(Math.round(mTemp)) + (char) 0x00B0;

                String check = Double.toString(currentTemp);

                //JSONObject mainObject = weatherObject.getJSONObject("main");
                //String temp = mainObject.getJSONObject("temp").toString();
                Log.i("Weatherobject: ", temp.toString());
                _mainTextView.setText(mCurrent + "");
                _speech.speak("The weather is" + mCurrent + "Fahrenheit", TextToSpeech.QUEUE_FLUSH, null);




            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    public static Location getLastLocation(Context context) {
        Location result = null;
        LocationManager locationManager;
        Criteria locationCriteria;
        List<String> providers;

        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        locationCriteria = new Criteria();
        locationCriteria.setAccuracy(Criteria.NO_REQUIREMENT);
        providers = locationManager.getProviders(locationCriteria, true);

        // Note that providers = locatoinManager.getAllProviders(); is not used because the
        // list might contain disabled providers or providers that are not allowed to be called.

        //Note that getAccuracy can return 0, indicating that there is no known accuracy.

        for (String provider : providers) {
            Location location = locationManager.getLastKnownLocation(provider);
            if (result == null) {
                result = location;
            }
            else if (result.getAccuracy() == 0.0) {
                if (location.getAccuracy() != 0.0) {
                    result = location;
                    break;
                } else {
                    if (result.getAccuracy() > location.getAccuracy()) {
                        result = location;
                    }
                }
            }
        }

        return result;
    }


    private Drawable loadImageFromWeb(String url)
    {
        try
        {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "src");
            return d;
        }catch (Exception e) {
            System.out.println("Exc="+e);
            return null;
        }
    }


}
